﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BancoBari.Sender.Domain.Commands
{
    public class CreateHelloCommand : HelloCommand
    {        
        public CreateHelloCommand(string requestId, string serviceName, string message, DateTime timeStamp)
        {
            RequestId = requestId;
            ServiceName = serviceName;
            Message = message;
            TimeStamp = timeStamp;
        }
    }
}
