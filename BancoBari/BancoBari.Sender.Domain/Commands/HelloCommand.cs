﻿using BancoBari.Domain.Core.Commands;
using System;

namespace BancoBari.Sender.Domain.Commands
{
    public abstract class HelloCommand : Command
    {
        public string RequestId { get; set; }
        public string ServiceName { get; set; }
        public string Message { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
