﻿using BancoBari.Domain.Core.Bus;
using BancoBari.Receiver.Domain.Events;
using BancoBari.Sender.Domain.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BancoBari.Sender.Domain.CommandHandlers
{
    public class HelloCommandHandler : IRequestHandler<CreateHelloCommand, bool>
    {
        private readonly IEventBus _bus;
        public HelloCommandHandler(IEventBus bus)
        {
            _bus = bus;
        }

        public Task<bool> Handle(CreateHelloCommand request, CancellationToken cancellationToken)
        {
            _bus.Publish(new HelloCreatedEvent(request.RequestId, request.ServiceName, request.Message, request.TimeStamp));
            return Task.FromResult(true);
        }
    }
}
