using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BancoBari.Sender.Application.Interfaces;
using BancoBari.Sender.Application.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace BancoBari.Sender.Worker
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IHelloService _helloService;

        public Worker(ILogger<Worker> logger, IHelloService helloService)
        {
            _logger = logger;
            _helloService = helloService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                _helloService.SendHello(new HelloMessage()
                {
                    RequestId = Guid.NewGuid().ToString(),
                    ServiceName = "Worker",
                    Message = "Hello World!",
                    Timestamp = DateTime.Now
                });
                await Task.Delay(5000, stoppingToken);
            }
        }
    }
}
