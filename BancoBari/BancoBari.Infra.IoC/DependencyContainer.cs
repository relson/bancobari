﻿using BancoBari.Domain.Core.Bus;
using BancoBari.Infra.Bus;
using BancoBari.Receiver.Domain.EventHandlers;
using BancoBari.Receiver.Domain.Events;
using BancoBari.Sender.Application.Interfaces;
using BancoBari.Sender.Application.Service;
using BancoBari.Sender.Domain.CommandHandlers;
using BancoBari.Sender.Domain.Commands;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace BancoBari.Infra.IoC
{
    public class DependencyContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<IEventBus, RabbitMQBus>(sp =>
            {
                var scopeFactory = sp.GetRequiredService<IServiceScopeFactory>();
                return new RabbitMQBus(sp.GetService<IMediator>(), scopeFactory);
            });
            
            services.AddTransient<IHelloService, HelloService>();

            services.AddTransient<HelloEventHandler>();
            services.AddTransient<IRequestHandler<CreateHelloCommand, bool>, HelloCommandHandler>();
        }
    }
}
