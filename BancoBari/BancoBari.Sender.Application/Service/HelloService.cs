﻿using BancoBari.Domain.Core.Bus;
using BancoBari.Sender.Application.Interfaces;
using BancoBari.Sender.Application.Models;
using BancoBari.Sender.Domain.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace BancoBari.Sender.Application.Service
{
    public class HelloService : IHelloService
    {
        private readonly IEventBus _bus;

        public HelloService(IEventBus bus)
        {
            _bus = bus;
        }

        public void SendHello(HelloMessage helloMessage)
        {
            var createHelloMessageCommand = new CreateHelloCommand(
                    helloMessage.RequestId,
                    helloMessage.ServiceName,
                    helloMessage.Message,
                    helloMessage.Timestamp
                );

            _bus.SendCommand(createHelloMessageCommand);
        }
    }
}
