﻿using BancoBari.Sender.Application.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BancoBari.Sender.Application.Interfaces
{
    public interface IHelloService
    {
        void SendHello(HelloMessage helloMessage);
    }
}
