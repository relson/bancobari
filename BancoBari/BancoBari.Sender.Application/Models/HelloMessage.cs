﻿using System;

namespace BancoBari.Sender.Application.Models
{
    public class HelloMessage
    {
        public string RequestId { get; set; }
        public string ServiceName { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
