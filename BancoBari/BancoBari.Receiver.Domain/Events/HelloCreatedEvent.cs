﻿using BancoBari.Domain.Core.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace BancoBari.Receiver.Domain.Events
{    
    public class HelloCreatedEvent : Event
    {
        public string RequestId { get; set; }
        public string ServiceName { get; set; }
        public string Message { get; set; }
        public DateTime TimeStamp { get; set; }

        public HelloCreatedEvent(string requestId, string serviceName, string message, DateTime timeStamp)
        {
            RequestId = requestId;
            ServiceName = serviceName;
            Message = message;
            Timestamp = timeStamp;
        }
    }
}
