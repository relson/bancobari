﻿using BancoBari.Domain.Core.Bus;
using BancoBari.Receiver.Domain.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BancoBari.Receiver.Domain.EventHandlers
{
    public class HelloEventHandler : IEventHandler<HelloCreatedEvent>
    {
        public HelloEventHandler()
        {

        }

        public Task Handle(HelloCreatedEvent evt)
        {
            Console.WriteLine($"RequestId: {evt.RequestId}");
            Console.WriteLine($"ServiceName: {evt.ServiceName}");
            Console.WriteLine($"Message: {evt.Message}");
            Console.WriteLine($"TimeStamp: {evt.TimeStamp}");
            Console.WriteLine("--------");
            return Task.CompletedTask;
        }
    }
}
