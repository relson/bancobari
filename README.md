# BancoBari - Desafio Técnico

Instruções de como executar a Solução.

## Dependências

É necessário que o RabbitMQ esteja em execução na porta padrão.

```
//localhost:5672/
```

Uma fila  ```HelloCreatedEvent``` será criada pelo microserviço automaticamente quando for executado.
![picture](Doc/03-rabbit.png)
### Executando a Solução

A Solução foi desenvolvida utilizando o ```Visual Studio 2019``` e ```.net core 3.1```.

Configurando a solução como ```Multiple Startup Projects```.

![picture](Doc/01-mult.png)

Os projetos ```Sender.Worker``` e ```Receiver.Api``` configurados como ```Start```.

![picture](Doc/02-mult.png)

Ao executar será possivel monitorar as mensagens enviadas pelo ```Sender.Worker```.

![picture](Doc/04-sender.worker.png)

e o obtidas pelo Subscriber ```Receiver.Api```

![picture](Doc/05-receiver.api.png)


### Melhorias 
* Configurar os microseviçoes para executar em um container e assim não ser necessário nenhuma dependência externa
* Utilizar o ```Serilog``` para logging
* Tratar e monitorar logs com ferramentas como ```prometheus``` e ```grafana```